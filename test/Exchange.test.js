const { balance, BN, ether, expectEvent, shouldFail, time, constants } = require('openzeppelin-test-helpers');

const { toHex } = web3.utils;

const LandRegistry = artifacts.require('LandRegistry');
const LandRegistryProxy = artifacts.require('LandRegistryProxy');
const KyberNetworkProxy = artifacts.require('KyberNetworkProxy');
const MoneyMarket = artifacts.require('MoneyMarket');
const PaymentsLayer = artifacts.require('PaymentsLayer');
const TokenizedProperty = artifacts.require('TokenizedProperty');
const Whitelist = artifacts.require('Whitelist');
const WhitelistProxy = artifacts.require('WhitelistProxy');

const Exchange = artifacts.require('Exchange');

contract('Exchange', function ([blockimmo, from, investor, purchaser, other, unauthorized]) {
  const eGrid = 'CH123456789012';
  const grundstuckNumber = 'CH-ZG1234';

  const ETHER_TOKEN_ADDRESS = '0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE';

  before(async function () {
    this.landRegistryProxy = await LandRegistryProxy.new({ from: blockimmo });
    this.landRegistryProxy.address.toLowerCase().should.be.equal('0x0f5ea0a652e851678ebf77b69484bfcd31f9459b');

    this.whitelistProxy = await WhitelistProxy.new({ from: blockimmo });
    this.whitelistProxy.address.toLowerCase().should.be.equal('0xec8be1a5630364292e56d01129e8ee8a9578d7d8');

    this.landRegistry = await LandRegistry.new({ from: blockimmo });
    await this.landRegistryProxy.set(this.landRegistry.address);
    (await this.landRegistryProxy.landRegistry.call()).should.be.equal(this.landRegistry.address);

    this.whitelist = await Whitelist.new({ from: blockimmo });
    await this.whitelistProxy.set(this.whitelist.address);
    (await this.whitelistProxy.whitelist.call()).should.be.equal(this.whitelist.address);

    this.fundingToken = await TokenizedProperty.new('DAI', 'DAI', { from: purchaser });
    await this.landRegistry.tokenizeProperty('DAI', this.fundingToken.address, { from: blockimmo });
    this.fundingToken.address.should.be.equal('0x9Ad61E35f8309aF944136283157FABCc5AD371E5');

    this.moneyMarket = await MoneyMarket.new();
    this.moneyMarket.address.should.be.equal('0x6732c278C58FC90542cce498981844A073D693d7');

    this.kyberNetworkProxy = await KyberNetworkProxy.new({ from: blockimmo });
    this.paymentsLayer = await PaymentsLayer.new({ from: blockimmo });

    await this.whitelist.grantPermissionBatch([investor, purchaser, this.moneyMarket.address, from, blockimmo, this.kyberNetworkProxy.address, this.paymentsLayer.address, other], 'authorized', { from: blockimmo });
    this.fundingToken.transfer(this.kyberNetworkProxy.address, ether('100'), { from: purchaser });

    this.exchange = await Exchange.new();
    // await this.exchange.pause();
    await this.whitelist.grantPermission(this.exchange.address, 'authorized');

    this.tokenizedProperty = await TokenizedProperty.new('CH12345678910', 'CH-ZG1234', { from });
    await this.landRegistry.tokenizeProperty('CH12345678910', this.tokenizedProperty.address, { from: blockimmo });

    this.defaultClosingTime = (await time.latest()).add(time.duration.days(1));
  });

  context('token is not whitelisted', function () {
    it('', async function () {
      await shouldFail.reverting(this.exchange.postOrder(false, this.defaultClosingTime, from, ether('1'), ether('1'), this.tokenizedProperty.address, { from }));
    });
  });

  context('token is whitelisted', function () {
    before(async function () {
      await this.whitelist.grantPermission(this.tokenizedProperty.address, 'authorized', { from: blockimmo });
    });

    it('invalid closing time', async function () {
      await shouldFail.reverting(this.exchange.postOrder(false, (await time.latest()).sub(time.duration.seconds(1)), from, ether('1'), ether('1'), this.tokenizedProperty.address, { from }));
    });

    it('invalid number of tokens', async function () {
      await shouldFail.reverting(this.exchange.postOrder(false, this.defaultClosingTime, from, 0, ether('1'), this.tokenizedProperty.address, { from }));
    });

    it('invalid price wei', async function () {
      await shouldFail.reverting(this.exchange.postOrder(false, this.defaultClosingTime, from, ether('1'), 0, this.tokenizedProperty.address, { from }));
    });

    it('seller does not hold enough tokens to fill this order', async function () {
      await shouldFail.reverting(this.exchange.postOrder(false, this.defaultClosingTime, from, ether('1'), ether('1100000'), this.tokenizedProperty.address, { from }));
    });

    it('seller has not approved enough tokens to fill this order', async function () {
      await shouldFail.reverting(this.exchange.postOrder(false, this.defaultClosingTime, from, ether('1'), ether('1'), this.tokenizedProperty.address, { from }));
    });

    it('invalid price wei and value', async function () {
      await shouldFail.reverting(this.exchange.postOrder(true, this.defaultClosingTime, other, ether('1'), ether('1'), this.tokenizedProperty.address, { from: other, value: ether('1.1') }));
    });
  });

  context('allowance is approved', function () {
    before(async function () {
      await this.tokenizedProperty.approve(this.exchange.address, ether('1'), { from });
    });

    it('posts an order', async function () {
      const { logs } = await this.exchange.postOrder(false, this.defaultClosingTime, from, ether('1'), ether('1'), this.tokenizedProperty.address, { from });
      expectEvent.inLogs(logs, 'OrderPosted', {
        closingTime: this.defaultClosingTime,
        numberOfTokens: ether('1'),
        numberOfDai: ether('1'),
        token: this.tokenizedProperty.address,
        from,
      });

      const { order } = logs[0].args;
      await this.exchange.deleteOrder(order, { from });
    });

    it('deletes an order', async function () {
      const { logs } = await this.exchange.postOrder(false, this.defaultClosingTime, from, ether('1'), ether('1'), this.tokenizedProperty.address, { from });
      const { order } = logs[0].args;

      const r = await this.exchange.deleteOrder(order, { from });
      r.logs[0].event.should.be.equal('OrderDeleted');
      r.logs[0].args.order.should.be.equal(order);
    });

    it('another account can\'t delete a valid order', async function () {
      const { logs } = await this.exchange.postOrder(false, this.defaultClosingTime, from, ether('1'), ether('1'), this.tokenizedProperty.address, { from });
      const { order } = logs[0].args;

      await shouldFail.reverting(this.exchange.deleteOrder(order, { from: other }));
      await this.exchange.deleteOrder(order, { from });
    });

    it('duplicate orders', async function () {
      await this.exchange.postOrder(false, this.defaultClosingTime, from, ether('1'), ether('1'), this.tokenizedProperty.address, { from });
      await shouldFail.reverting(this.exchange.postOrder(false, this.defaultClosingTime, from, ether('1'), ether('1'), this.tokenizedProperty.address, { from }));
    });

    it('multiple orders', async function () {
      const { logs } = await this.exchange.postOrder(false, this.defaultClosingTime, from, ether('.1'), ether('1'), this.tokenizedProperty.address, { from });
      logs[0].event.should.be.equal('OrderPosted');
      const { order } = logs[0].args;

      const r = await this.exchange.postOrder(false, this.defaultClosingTime, from, ether('.9'), ether('1'), this.tokenizedProperty.address, { from });
      const _order = r.logs[0].args.order;

      order.should.not.equal(_order);
    });
  });

  context('trading', function () {
    beforeEach(async function () {
      this.tP = await TokenizedProperty.new(eGrid, grundstuckNumber, { from });
      await this.landRegistry.tokenizeProperty(eGrid, this.tP.address, { from: blockimmo });
      await this.whitelist.grantPermission(this.tP.address, 'authorized', { from: blockimmo });
    });

    afterEach(async function () {
      await this.landRegistry.untokenizeProperty(eGrid, { from: blockimmo });
      const balance = await this.fundingToken.balanceOf(from);
      if (balance > 0) {
        await this.fundingToken.transfer(purchaser, balance, { from });
      }
    });

    it('empty', async function () {
      await this.tP.approve(this.exchange.address, ether('1'), { from });
      const { logs } = await this.exchange.postOrder(false, this.defaultClosingTime, from, ether('1'), ether('1'), this.tP.address, { from, gasPrice: 0 });

      const { order } = logs[0].args;
      (await this.exchange.isValid(order)).should.be.equal(true);

      await this.exchange.fillOrders([], other, 0, 0, { from: other, gasPrice: 0 });

      // (await this.tP.balanceOf.call(from)).should.be.bignumber.equal(ether('1000000'));
      (await this.tP.balanceOf.call(other)).should.be.bignumber.equal(new BN(0));
    });

    it('fills an order dai', async function () {
      await this.tP.approve(this.exchange.address, ether('1'), { from });
      const { logs } = await this.exchange.postOrder(false, this.defaultClosingTime, from, ether('1'), ether('1'), this.tP.address, { from, gasPrice: 0 });

      const { order } = logs[0].args;
      (await this.exchange.isValid(order)).should.be.equal(true);

      await this.fundingToken.approve(this.exchange.address, ether('1'), { from: purchaser });
      await this.exchange.fillOrders([order], purchaser, toHex(0), ether('1'), { from: purchaser, gasPrice: 0 });

      // (await this.tP.balanceOf.call(from)).should.be.bignumber.equal(ether('1000000').sub(ether('1')));
      (await this.tP.balanceOf.call(purchaser)).should.be.bignumber.equal(ether('1'));
    });

    it('fills an order ether', async function () {
      await this.tP.approve(this.exchange.address, ether('1'), { from });
      const { logs } = await this.exchange.postOrder(false, this.defaultClosingTime, from, ether('1'), ether('1'), this.tP.address, { from, gasPrice: 0 });

      const { order } = logs[0].args;
      (await this.exchange.isValid(order)).should.be.equal(true);

      const encodedFunctionCall = web3.eth.abi.encodeFunctionCall({
        name: 'fillOrders',
        type: 'function',
        inputs: [
          { type: 'bytes32[]', name: '_hashes' },
          { type: 'address', name: '_from' },
          { type: 'uint256', name: '_numberOfTokens' },
          { type: 'uint256', name: '_numberOfDai' },
        ],
      }, [[order], other, toHex(0), toHex(ether('1'))]);
      await this.paymentsLayer.forwardPayment(this.kyberNetworkProxy.address, ETHER_TOKEN_ADDRESS, 1, this.exchange.address, encodedFunctionCall, { from: other, value: ether('1') });

      // (await this.tP.balanceOf.call(from)).should.be.bignumber.equal(ether('1000000').sub(ether('1')));
      (await this.tP.balanceOf.call(other)).should.be.bignumber.equal(ether('1'));
      (await this.fundingToken.balanceOf(from)).should.be.bignumber.equal(ether('1'));
    });

    it('fills an order different token', async function () {
      await this.tP.approve(this.exchange.address, ether('1'), { from });
      const { logs } = await this.exchange.postOrder(false, this.defaultClosingTime, from, ether('1'), ether('1'), this.tP.address, { from, gasPrice: 0 });

      const { order } = logs[0].args;
      (await this.exchange.isValid(order)).should.be.equal(true);

      await this.fundingToken.approve(this.paymentsLayer.address, ether('1'), { from: purchaser });
      const encodedFunctionCall = web3.eth.abi.encodeFunctionCall({
        name: 'fillOrders',
        type: 'function',
        inputs: [
          { type: 'bytes32[]', name: '_hashes' },
          { type: 'address', name: '_from' },
          { type: 'uint256', name: '_numberOfTokens' },
          { type: 'uint256', name: '_numberOfDai' },
        ],
      }, [[order], other, toHex(0), toHex(ether('1'))]);
      await this.paymentsLayer.forwardPayment(this.kyberNetworkProxy.address, this.fundingToken.address, 1, this.exchange.address, encodedFunctionCall, { from: purchaser });

      // (await this.tP.balanceOf.call(from)).should.be.bignumber.equal(ether('1000000').sub(ether('1')));
      (await this.tP.balanceOf.call(other)).should.be.bignumber.equal(ether('1'));
      (await this.fundingToken.balanceOf(from)).should.be.bignumber.equal(ether('1'));
    });

    it('partially fills an order', async function () {
      await this.tP.approve(this.exchange.address, ether('1'), { from });
      const { logs } = await this.exchange.postOrder(false, this.defaultClosingTime, from, ether('1'), ether('1'), this.tP.address, { from, gasPrice: 0 });
      const { order } = logs[0].args;

      const encodedFunctionCall = web3.eth.abi.encodeFunctionCall({
        name: 'fillOrders',
        type: 'function',
        inputs: [
          { type: 'bytes32[]', name: '_hashes' },
          { type: 'address', name: '_from' },
          { type: 'uint256', name: '_numberOfTokens' },
          { type: 'uint256', name: '_numberOfDai' },
        ],
      }, [[order], other, toHex(0), toHex(ether('1').divn(2))]);
      await this.paymentsLayer.forwardPayment(this.kyberNetworkProxy.address, ETHER_TOKEN_ADDRESS, 1, this.exchange.address, encodedFunctionCall, { from: other, value: ether('1').divn(2) });

      (await this.exchange.isValid(order)).should.be.equal(false);

      (await this.tP.balanceOf.call(other)).should.be.bignumber.equal(ether('1').divn(2));
      (await this.fundingToken.balanceOf(from)).should.be.bignumber.equal(ether('1').divn(2));
    });

    it('order filled by multiple parties', async function () {
      await this.tP.approve(this.exchange.address, ether('1'), { from });
      const { logs } = await this.exchange.postOrder(false, this.defaultClosingTime, from, ether('1'), ether('1'), this.tP.address, { from, gasPrice: 0 });

      const { order } = logs[0].args;
      (await this.exchange.isValid(order)).should.be.equal(true);

      await this.fundingToken.approve(this.exchange.address, ether('1').divn(2), { from: purchaser });
      const r = await this.exchange.fillOrders([order], purchaser, toHex(0), ether('1').divn(2), { from: purchaser, gasPrice: 0 });
      r.logs[0].event.should.be.equal('OrderFilled');
      r.logs[1].event.should.be.equal('OrderDeleted');
      r.logs[2].event.should.be.equal('OrderPosted');

      const _order = r.logs[2].args.order;
      (await this.exchange.isValid(_order)).should.be.equal(true);

      const encodedFunctionCall = web3.eth.abi.encodeFunctionCall({
        name: 'fillOrders',
        type: 'function',
        inputs: [
          { type: 'bytes32[]', name: '_hashes' },
          { type: 'address', name: '_from' },
          { type: 'uint256', name: '_numberOfTokens' },
          { type: 'uint256', name: '_numberOfDai' },
        ],
      }, [[_order], other, toHex(0), toHex(ether('1').divn(2))]);
      await this.paymentsLayer.forwardPayment(this.kyberNetworkProxy.address, ETHER_TOKEN_ADDRESS, 1, this.exchange.address, encodedFunctionCall, { from: other, value: ether('1').divn(2) });

      (await this.tP.balanceOf.call(other)).should.be.bignumber.equal(ether('1').divn(2));
      (await this.tP.balanceOf.call(purchaser)).should.be.bignumber.equal(ether('1').divn(2));
      (await this.fundingToken.balanceOf.call(from)).should.be.bignumber.equal(ether('1'));
    });

    it('buyer is refunded change', async function () {
      await this.tP.approve(this.exchange.address, ether('1'), { from });
      const { logs } = await this.exchange.postOrder(false, this.defaultClosingTime, from, ether('1'), ether('1').divn(2), this.tP.address, { from, gasPrice: 0 });
      const { order } = logs[0].args;

      const balance = await this.fundingToken.balanceOf.call(purchaser);

      await this.fundingToken.approve(this.exchange.address, ether('1'), { from: purchaser });
      await this.exchange.fillOrders([order], purchaser, toHex(0), ether('1'), { from: purchaser, gasPrice: 0 });

      balance.sub(await this.fundingToken.balanceOf.call(purchaser)).should.be.bignumber.equal(ether('1').divn(2));
      (await this.tP.balanceOf.call(purchaser)).should.be.bignumber.equal(ether('1'));
    });

    it('buyer is refunded change ether', async function () {
      await this.tP.approve(this.exchange.address, ether('1'), { from });
      const { logs } = await this.exchange.postOrder(false, this.defaultClosingTime, from, ether('1'), ether('1').divn(2), this.tP.address, { from, gasPrice: 0 });
      const { order } = logs[0].args;

      const balance = await this.fundingToken.balanceOf(other);

      const encodedFunctionCall = web3.eth.abi.encodeFunctionCall({
        name: 'fillOrders',
        type: 'function',
        inputs: [
          { type: 'bytes32[]', name: '_hashes' },
          { type: 'address', name: '_from' },
          { type: 'uint256', name: '_numberOfTokens' },
          { type: 'uint256', name: '_numberOfDai' },
        ],
      }, [[order], other, toHex(0), toHex(ether('1'))]);
      const r = await this.paymentsLayer.forwardPayment(this.kyberNetworkProxy.address, ETHER_TOKEN_ADDRESS, 1, this.exchange.address, encodedFunctionCall, { from: other, value: ether('1') });
      expectEvent.inLogs(r.logs, 'PaymentForwarded', {
        from: other,
        to: this.exchange.address,
        srcToken: ETHER_TOKEN_ADDRESS,
        amountDai: ether('1').divn(2),
        amountSrc: ether('1'),
        changeDai: ether('1').divn(2),
      });

      (await this.fundingToken.balanceOf(other)).sub(balance).should.be.bignumber.equal(ether('1').divn(2));
      (await this.tP.balanceOf(other)).should.be.bignumber.equal(ether('1'));
    });

    it('fills multiple orders', async function () {
      await this.tP.approve(this.exchange.address, ether('1'), { from });
      const { logs } = await this.exchange.postOrder(false, this.defaultClosingTime, from, ether('.1'), ether('.1'), this.tP.address, { from, gasPrice: 0 });
      const { order } = logs[0].args;

      const r = await this.exchange.postOrder(false, this.defaultClosingTime, from, ether('.9'), ether('.9'), this.tP.address, { from, gasPrice: 0 });
      const _order = r.logs[0].args.order;

      await this.fundingToken.approve(this.exchange.address, ether('1'), { from: purchaser });
      await this.exchange.fillOrders([order, _order], purchaser, toHex(0), ether('1'), { from: purchaser, gasPrice: 0 });

      (await this.fundingToken.balanceOf(from)).should.be.bignumber.equal(ether('1'));
      (await this.tP.balanceOf(purchaser)).should.be.bignumber.equal(ether('1'));
    });

    it('non whitelisted address can\'t fill an order', async function () {
      await this.tP.approve(this.exchange.address, ether('1'), { from });
      const { logs } = await this.exchange.postOrder(false, this.defaultClosingTime, from, ether('1'), ether('1'), this.tP.address, { from, gasPrice: 0 });
      const { order } = logs[0].args;
      await shouldFail.reverting(this.exchange.fillOrders([order], unauthorized, 0, ether('1'), { from: unauthorized, value: ether('1') }));
    });

    it('fills multiple orders', async function () {
      await this.tP.transfer(other, ether('1'), { from });
      await this.tP.approve(this.exchange.address, ether('1'), { from });
      await this.tP.approve(this.exchange.address, ether('1'), { from: other });

      const { logs } = await this.exchange.postOrder(false, this.defaultClosingTime, from, ether('1'), ether('1'), this.tP.address, { from, gasPrice: 0 });
      const { order } = logs[0].args;

      const r = await this.exchange.postOrder(false, this.defaultClosingTime, other, ether('1'), ether('2'), this.tP.address, { from: other, gasPrice: 0 });
      const _order = r.logs[0].args.order;

      await this.fundingToken.approve(this.exchange.address, ether('3'), { from: purchaser });
      await this.exchange.fillOrders([order, _order], purchaser, toHex(0), ether('3'), { from: purchaser, gasPrice: 0 });

      (await this.fundingToken.balanceOf(from)).should.be.bignumber.equal(ether('1'));
      (await this.tP.balanceOf(purchaser)).should.be.bignumber.equal(ether('2'));
    });
  });

  context('buy orders', function () {
    beforeEach(async function () {
      this.tP = await TokenizedProperty.new(eGrid, grundstuckNumber, { from });
      await this.landRegistry.tokenizeProperty(eGrid, this.tP.address, { from: blockimmo });
      await this.whitelist.grantPermission(this.tP.address, 'authorized', { from: blockimmo });
    });

    afterEach(async function () {
      await this.landRegistry.untokenizeProperty(eGrid, { from: blockimmo });

      const balance = await this.fundingToken.balanceOf(from);
      if (balance > 0) { await this.fundingToken.transfer(purchaser, balance, { from }); }
    });

    it('empty', async function () {
      await this.fundingToken.approve(this.exchange.address, ether('1'), { from: purchaser });
      const { logs } = await this.exchange.postOrder(true, this.defaultClosingTime, purchaser, ether('1'), ether('1'), this.tP.address, { from: purchaser, gasPrice: 0 });

      const { order } = logs[1].args;
      (await this.exchange.isValid(order)).should.be.equal(true);

      await this.tP.approve(this.exchange.address, ether('1'), { from });
      await this.exchange.fillOrders([], from, toHex(0), toHex(0), { from, gasPrice: 0 });

      // (+(await this.tP.balanceOf.call(from))).should.be.equal(ether('1000000'));
      (await this.tP.balanceOf.call(other)).should.be.bignumber.equal(new BN(0));
    });

    it('fills an order', async function () {
      await this.fundingToken.approve(this.exchange.address, ether('1'), { from: purchaser });
      const { logs } = await this.exchange.postOrder(true, this.defaultClosingTime, purchaser, ether('1'), ether('1'), this.tP.address, { from: purchaser, gasPrice: 0 });

      const { order } = logs[1].args;
      (await this.exchange.isValid(order)).should.be.equal(true);

      await this.tP.approve(this.exchange.address, ether('1'), { from });
      await this.exchange.fillOrders([order], from, ether('1'), toHex(0), { from, gasPrice: 0 });

      (await this.fundingToken.balanceOf.call(from)).should.be.bignumber.equal(ether('1'));
      (await this.tP.balanceOf.call(purchaser)).should.be.bignumber.equal(ether('1'));
    });

    it('partially fills an order', async function () {
      await this.fundingToken.approve(this.exchange.address, ether('1'), { from: purchaser });
      const { logs } = await this.exchange.postOrder(true, this.defaultClosingTime, purchaser, ether('1'), ether('1'), this.tP.address, { from: purchaser, gasPrice: 0 });

      const { order } = logs[1].args;
      (await this.exchange.isValid(order)).should.be.equal(true);

      await this.tP.approve(this.exchange.address, ether('1'), { from });
      await this.exchange.fillOrders([order], from, ether('1').divn(2), toHex(0), { from, gasPrice: 0 });

      (await this.fundingToken.balanceOf.call(from)).should.be.bignumber.equal(ether('1').divn(2));
      (await this.tP.balanceOf.call(purchaser)).should.be.bignumber.equal(ether('1').divn(2));
    });

    it('posts an order ether', async function () {
      const encodedFunctionCall = web3.eth.abi.encodeFunctionCall({
        name: 'postOrder',
        type: 'function',
        inputs: [
          { type: 'bool', name: '_buy' },
          { type: 'uint256', name: '_closingTime' },
          { type: 'address', name: '_from' },
          { type: 'uint256', name: '_numberOfTokens' },
          { type: 'uint256', name: '_numberOfDai' },
          { type: 'address', name: '_token' },
        ],
      }, [true, toHex(this.defaultClosingTime), other, toHex(ether('1')), toHex(ether('1')), this.tP.address]);
      await this.paymentsLayer.forwardPayment(this.kyberNetworkProxy.address, ETHER_TOKEN_ADDRESS, 1, this.exchange.address, encodedFunctionCall, { from: other, value: ether('1') });

      (await this.exchange.deposits(other)).should.be.bignumber.equal(ether('1'));

      // (await this.tP.balanceOf.call(from)).should.be.bignumber.equal(ether('1000000').sub(ether('1')));
      // (await this.tP.balanceOf.call(other)).should.be.bignumber.equal(ether('1'));
    });

    it('order filled by multiple parties', async function () {
      await this.fundingToken.approve(this.exchange.address, ether('1'), { from: purchaser });
      const { logs } = await this.exchange.postOrder(true, this.defaultClosingTime, purchaser, ether('1'), ether('1'), this.tP.address, { from: purchaser, gasPrice: 0 });
      const { order } = logs[1].args;

      await this.tP.approve(this.exchange.address, ether('1'), { from, gasPrice: 0 });
      const r = await this.exchange.fillOrders([order], from, ether('1').divn(2), toHex(0), { from });
      r.logs[0].event.should.be.equal('Pulled');
      r.logs[1].event.should.be.equal('OrderFilled');
      r.logs[2].event.should.be.equal('OrderDeleted');
      r.logs[3].event.should.be.equal('OrderPosted');

      const _order = r.logs[3].args.order;
      (await this.exchange.isValid(_order)).should.be.equal(true);

      await this.exchange.fillOrders([_order], from, ether('1').divn(2), toHex(0), { from });

      (await this.tP.balanceOf(purchaser)).should.be.bignumber.equal(ether('1'));
    });

    /* it('fills multiple orders', async function () {
      const { logs } = await this.exchange.postOrder(true, this.defaultClosingTime, other, ether('1'), ether('1'), this.tP.address, { from: other, gasPrice: 0, value: ether('1') });
      const { order } = logs[0].args;

      const r = await this.exchange.postOrder(true, this.defaultClosingTime, unauthorized, ether('1'), ether('2'), this.tP.address, { from: unauthorized, gasPrice: 0, value: ether('2') });
      const _order = r.logs[0].args.order;

      await this.whitelist.grantPermission(unauthorized, 'authorized');

      await this.tP.approve(this.exchange.address, 2e18, { from });

      (+(await balance.difference(from, () =>
        this.exchange.fillOrders([order, _order], from, 2e18, { from, gasPrice: 0 }))
      )).should.be.equal(3e18);

      (+(await this.tP.balanceOf.call(from))).should.be.equal(ether('1000000').sub(ether('2')));
      (+(await this.tP.balanceOf.call(other))).should.be.equal(ether('1'));
      (+(await this.tP.balanceOf.call(unauthorized))).should.be.equal(ether('1'));
    }); */

    it('deletes an order', async function () {
      const balance = await this.fundingToken.balanceOf(purchaser);

      await this.fundingToken.approve(this.exchange.address, ether('1'), { from: purchaser });
      const { logs } = await this.exchange.postOrder(true, this.defaultClosingTime, purchaser, ether('1'), ether('1'), this.tP.address, { from: purchaser, gasPrice: 0 });
      const { order } = logs[1].args;
      (await this.exchange.isValid(order)).should.be.equal(true);

      await this.exchange.deleteOrder(order, { from: purchaser, gasPrice: 0 });

      balance.sub(await this.fundingToken.balanceOf(purchaser)).should.be.bignumber.equal(new BN(0));
    });
  });
});
