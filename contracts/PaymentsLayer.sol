pragma solidity 0.5.6;

import "openzeppelin-solidity/contracts/math/SafeMath.sol";
import "openzeppelin-solidity/contracts/token/ERC20/SafeERC20.sol";
import "openzeppelin-solidity/contracts/utils/ReentrancyGuard.sol";

contract KyberNetworkProxyInterface {
  function getExpectedRate(IERC20 src, IERC20 dest, uint srcQty) public view returns (uint expectedRate, uint slippageRate);
  function swapEtherToToken(IERC20 token, uint minConversionRate) public payable returns (uint);
  function swapTokenToToken(IERC20 src, uint srcAmount, IERC20 dest, uint minConversionRate) public returns (uint);
}

contract PaymentsLayer is ReentrancyGuard {
  using SafeERC20 for IERC20;
  using SafeMath for uint256;

  IERC20 public dai = IERC20(0x9Ad61E35f8309aF944136283157FABCc5AD371E5);  // 0x89d24A6b4CcB1B6fAA2625fE562bDD9a23260359
  address public constant ETH_TOKEN_ADDRESS = 0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE;

  event PaymentForwarded(address indexed from, address indexed to, address indexed srcToken, uint256 amountDai, uint256 amountSrc, uint256 changeDai, bytes encodedFunctionCall);

  function forwardPayment(KyberNetworkProxyInterface _kyberNetworkProxy, IERC20 _srcToken, uint256 _minimumRate, address _destinationAddress, bytes memory _encodedFunctionCall) public nonReentrant payable {
    require(address(_srcToken) != address(0) && _minimumRate > 0 && _destinationAddress != address(0), "invalid parameter(s)");

    uint256 srcQuantity = address(_srcToken) == ETH_TOKEN_ADDRESS ? msg.value : _srcToken.allowance(msg.sender, address(this));

    if (address(_srcToken) != ETH_TOKEN_ADDRESS) {
      _srcToken.safeTransferFrom(msg.sender, address(this), srcQuantity);
      _srcToken.safeApprove(address(_kyberNetworkProxy), srcQuantity);
    }

    uint256 amountDai = address(_srcToken) == ETH_TOKEN_ADDRESS ? _kyberNetworkProxy.swapEtherToToken.value(srcQuantity)(dai, _minimumRate) : _kyberNetworkProxy.swapTokenToToken(_srcToken, srcQuantity, dai, _minimumRate);
    require(amountDai >= srcQuantity.mul(_minimumRate).div(1e18), "_kyberNetworkProxy failed");

    dai.safeApprove(_destinationAddress, amountDai);

    (bool success, ) = _destinationAddress.call(_encodedFunctionCall);
    require(success, "destination call failed");

    uint256 changeDai = dai.allowance(address(this), _destinationAddress);
    if (changeDai > 0)
      dai.safeTransfer(msg.sender, changeDai);
    require(dai.allowance(address(this), address(_destinationAddress)) == 0, "allowance not fully consumed by _destinationAddress");

    emit PaymentForwarded(msg.sender, _destinationAddress, address(_srcToken), amountDai.sub(changeDai), srcQuantity, changeDai, _encodedFunctionCall);
  }
}
