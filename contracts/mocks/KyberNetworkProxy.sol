pragma solidity 0.5.6;

import "openzeppelin-solidity/contracts/math/SafeMath.sol";
import "openzeppelin-solidity/contracts/token/ERC20/SafeERC20.sol";

contract KyberNetworkProxy {
  using SafeERC20 for IERC20;
  using SafeMath for uint256;

  function swapEtherToToken(IERC20 token, uint minConversionRate) public payable returns(uint) {
    token.safeTransfer(msg.sender, msg.value.mul(minConversionRate));
    return msg.value.mul(minConversionRate);
  }

  function swapTokenToToken(IERC20 src, uint srcAmount, IERC20 dest, uint minConversionRate) public returns (uint) {
    src.safeTransferFrom(msg.sender, address(this), srcAmount);
    dest.safeTransfer(msg.sender, srcAmount.mul(minConversionRate));
    return srcAmount.mul(minConversionRate);
  }
}
