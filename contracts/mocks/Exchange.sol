pragma solidity 0.5.6;

import "openzeppelin-solidity/contracts/math/Math.sol";
import "openzeppelin-solidity/contracts/math/SafeMath.sol";
import "openzeppelin-solidity/contracts/token/ERC20/SafeERC20.sol";

import "../LoanEscrow.sol";

contract WhitelistInterface {
  function hasRole(address _operator, string memory _role) public view returns (bool);
}

contract WhitelistProxyInterface {
  function owner() public view returns (address);
  function whitelist() public view returns (WhitelistInterface);
}

contract Exchange is LoanEscrow {
  using SafeERC20 for IERC20;
  using SafeMath for uint256;

  uint256 public constant POINTS = uint256(10) ** 32;
  WhitelistProxyInterface public whitelistProxy = WhitelistProxyInterface(0xEC8bE1A5630364292E56D01129E8ee8A9578d7D8);  // 0x77eb36579e77e6a4bcd2Ca923ada0705DE8b4114

  struct Order {
    bool buy;
    uint256 closingTime;
    uint256 numberOfTokens;
    uint256 numberOfDai;
    IERC20 token;
    address from;
  }

  mapping(bytes32 => Order) public orders;

  event OrderDeleted(bytes32 indexed order);
  event OrderFilled(bytes32 indexed order, uint256 numberOfTokens, uint256 numberOfDai, address indexed to);
  event OrderPosted(bytes32 indexed order, bool indexed buy, uint256 closingTime, uint256 numberOfTokens, uint256 numberOfDai, IERC20 indexed token, address from);

  function blockimmo() public view returns (address) {
    return whitelistProxy.owner();
  }

  function deleteOrder(bytes32 _hash) public {
    Order memory o = orders[_hash];
    require(o.from == msg.sender || !isValid(_hash));

    if (o.buy)
      pull(o.from, o.numberOfDai, true);

    _deleteOrder(_hash);
  }

  function fillOrders(bytes32[] memory _hashes, address _from, uint256 _numberOfTokens, uint256 _numberOfDai) public {
    uint256 remainingTokens = _numberOfTokens;
    uint256 remainingDai = _numberOfDai;

    for (uint256 i = 0; i < _hashes.length; i++) {
      bytes32 hash = _hashes[i];
      require(isValid(hash), "invalid order");

      Order memory o = orders[hash];

      uint256 coefficient = (o.buy ? remainingTokens : remainingDai).mul(POINTS).div(o.buy ? o.numberOfTokens : o.numberOfDai);

      uint256 nTokens = o.numberOfTokens.mul(Math.min(coefficient, POINTS)).div(POINTS);
      uint256 vDai = o.numberOfDai.mul(Math.min(coefficient, POINTS)).div(POINTS);

      o.buy ? remainingTokens -= nTokens : remainingDai -= vDai;
      o.buy ? pull(_from, vDai, false) : dai.safeTransferFrom(msg.sender, o.from, vDai);
      o.token.safeTransferFrom(o.buy ? _from : o.from, o.buy ? o.from : _from, nTokens);

      emit OrderFilled(hash, nTokens, vDai, _from);
      _deleteOrder(hash);

      if (coefficient < POINTS)
        _postOrder(o.buy, o.closingTime, o.numberOfTokens.sub(nTokens), o.numberOfDai.sub(vDai), o.token, o.from);
    }
  }

  function isValid(bytes32 _hash) public view returns (bool valid) {
    Order memory o = orders[_hash];

    valid = now <= o.closingTime && o.closingTime <= now.add(4 weeks);
    valid = valid && (o.buy || (o.token.balanceOf(o.from) >= o.numberOfTokens && o.token.allowance(o.from, address(this)) >= o.numberOfTokens));
    valid = valid && o.numberOfTokens > 0 && o.numberOfDai > 0;
    valid = valid && whitelistProxy.whitelist().hasRole(address(o.token), "authorized");
  }

  function postOrder(bool _buy, uint256 _closingTime, address _from, uint256 _numberOfTokens, uint256 _numberOfDai, IERC20 _token) public {
    if (_buy)
      deposit(_from, _numberOfDai);

    _postOrder(_buy, _closingTime, _numberOfTokens, _numberOfDai, _token, _from);
  }

  function _deleteOrder(bytes32 _hash) internal {
    delete orders[_hash];
    emit OrderDeleted(_hash);
  }

  function _postOrder(bool _buy, uint256 _closingTime, uint256 _numberOfTokens, uint256 _numberOfDai, IERC20 _token, address _from) internal {
    bytes32 hash = keccak256(abi.encodePacked(_buy, _closingTime, _numberOfTokens, _numberOfDai, _token, _from));
    require(orders[hash].closingTime == 0, "order exists");

    orders[hash] = Order(_buy, _closingTime, _numberOfTokens, _numberOfDai, _token, _from);

    require(isValid(hash), "invalid order");
    emit OrderPosted(hash, _buy, _closingTime, _numberOfTokens, _numberOfDai, _token, _from);
  }
}
